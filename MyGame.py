import arcade
import arcade.key
from models import World
 
SCREEN_WIDTH = 800  
SCREEN_HEIGHT = 600

class ModelSprite(arcade.Sprite):
    def __init__(self, *args, **kwargs):
        self.model = kwargs.pop('model', None)
 
        super().__init__(*args, **kwargs)
 
    def sync_with_model(self):
        if self.model:
            self.set_position(self.model.x, self.model.y)
            self.angle = self.model.angle
 
    def draw(self):
        self.sync_with_model()
        super().draw()
 
class GameWindow(arcade.Window):
    def __init__(self, width, height):
        super().__init__(width, height)
        self.total_time = 90.0
        arcade.set_background_color(arcade.color.BLACK)
       
        self.world = World(width,height)
        
        self.background_sprite = arcade.Sprite('images/space.png')
        self.background_sprite.set_position(width/2,height/2)
        self.player1_ship_sprite = ModelSprite('images/playerShip1_orange.png' ,model=self.world.player1_ship)
        self.player2_ship_sprite = ModelSprite('images/playerShip1_blue.png' ,model=self.world.player2_ship)
        self.silver_energy_sprite = ModelSprite('images/bold_silver.png',model=self.world.silver_energy)
        self.enermy_sprite = ModelSprite('images/enemyBlue1.png' ,model=self.world.enermy)
        self.miniShip1_sprite = ModelSprite('images/playerLife1_red.png' ,model=self.world.miniShip1)
        self.miniShip2_sprite = ModelSprite('images/playerLife1_blue.png' ,model=self.world.miniShip2)

        self.meteor_sprites = [] 
        for meteor in self.world.meteor:
            self.meteor_sprites.append(ModelSprite('images/meteorGrey_med1.png' ,model=meteor))
        
        self.energy_sprites = [] 
        for energy in self.world.energy:
            self.energy_sprites.append(ModelSprite('images/bolt_gold.png' ,model=energy))

    def on_draw(self):
        # Calculate minutes
        minutes = int(self.total_time) // 60

        # Calculate seconds by using a modulus (remainder)
        seconds = int(self.total_time) % 60
        arcade.start_render()
        self.background_sprite.draw()
        if self.total_time > 0 : 
            self.player1_ship_sprite.draw()
            self.player2_ship_sprite.draw()
            self.enermy_sprite.draw()
            self.silver_energy_sprite.draw()
            self.miniShip1_sprite.draw()
            self.miniShip2_sprite.draw()
            arcade.draw_text("PLAYER 2 Score: "+str(self.world.P1_SCORE),
                         self.width - 180, self.height - 30,
                         arcade.color.WHITE, 10)
            arcade.draw_text("PLAYER 1 Score: "+str(self.world.P2_SCORE),
                         20, self.height - 30,
                         arcade.color.WHITE, 10)  

            output = "{:02d}:{:02d}".format(minutes, seconds)
            arcade.draw_text(output, 350, self.height - 30, arcade.color.WHITE, 20)               

            for sprite in self.meteor_sprites:
                sprite.draw()
            for sprite in self.energy_sprites:
                sprite.draw()
        
        else: 
            output = "GAME OVER"
            if self.world.P1_SCORE > self.world.P2_SCORE:
                arcade.draw_text("PLAYER2 WIN",400,490,arcade.color.WHITE,30)
            elif self.world.P2_SCORE > self.world.P1_SCORE:
                arcade.draw_text("PLAYER1 WIN",400,490,arcade.color.WHITE,30)
                
            arcade.draw_text(output, 200, 300, arcade.color.WHITE, 50) 

    def animate(self, delta_time,):
        self.world.animate(delta_time)
        self.total_time -= delta_time
    
    def on_key_press(self, key, key_modifiers):
        self.world.on_key_press(key, key_modifiers)

    def on_key_release(self, key, key_modifiers):
        self.world.on_key_release(key, key_modifiers)

if __name__ == '__main__':
    window = GameWindow(SCREEN_WIDTH, SCREEN_HEIGHT)
    arcade.run()