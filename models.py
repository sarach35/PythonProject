import arcade.key
from random import randint

FAST_FALL_SPEED = 10 
MOVEMENT_SPEED = 5
METEOR_FALLSPEED = 3


class Model:
    def __init__(self, world, x, y, angle):
        self.world = world 
        self.x = x
        self.y = y
        self.vx = 0
        self.vy = 0
        self.angle = 0

    def hit(self, other, hit_size):
        return (abs(self.x - other.x) <= hit_size) and (abs(self.y - other.y) <= hit_size)
    
    def inArea(self, other):
        hit_size = 200
        return (abs(self.x - other.x) <= hit_size) and (abs(self.y - other.y) <= hit_size)
    
    def random_location(self):
        self.x = randint(0, self.world.width-1)
        self.y = self.world.height-1 

        

class Player1_Ship(Model): 
    def __init__(self, world, x, y):
        super().__init__(world, x,y,0)
    
    def move(self):
        self.x += self.vx
        self.y += self.vy
 
    def animate(self, delta):
        if self.y > self.world.height:
            self.y = 0
        elif self.y <= 0:
            self.y = self.world.height
        if self.x > self.world.width: 
            self.x = 0
        elif self.x <= 0: 
            self.x = self.world.width 
        self.move()

class Player2_Ship(Model): 
    def __init__(self, world, x, y):
        super().__init__(world, x,y,0)
    
    def move(self):
        self.x += self.vx
        self.y += self.vy
 
    def animate(self, delta):
        if self.y > self.world.height:
            self.y = 0
        elif self.y <= 0:
            self.y = self.world.height
        if self.x > self.world.width: 
            self.x = 0
        elif self.x <= 0: 
            self.x = self.world.width 
        self.move()

class Energy(Model):
    def __init__(self, world, x, y):
        super().__init__(world, x,y,0)
    
    def move(self):
        self.vy = MOVEMENT_SPEED

    def animate(self, delta):
        self.move()
        if self.y <= 0:
            self.random_location()
        self.y -= self.vy

class Silver_Energy(Model):
    def __init__(self, world, x, y):
        super().__init__(world, x,y,0)
    
    def move(self):
        self.vy = MOVEMENT_SPEED*2

    def animate(self, delta):
        self.move()
        if self.y <= 0:
            self.random_location()
        self.y -= self.vy

class Enermy(Model):
    def __init__(self, world, x, y):
            super().__init__(world, x,y,0)
    
    def move(self):
        self.vy = MOVEMENT_SPEED/10

    def animate(self, delta):
        self.move()
        if self.y <= 0:
            self.random_location()
        self.y -= self.vy

class Meteor(Model):
    def __init__(self, world, x, y):
        super().__init__(world, x,y,0)
    
    def pushMeteor(self):
        if self.inArea(self):
             self.vy = 15

    def move(self):
        self.vy = METEOR_FALLSPEED

    def animate(self, delta):
        self.move()
        if self.y <= 0:
            self.random_location()
        self.y -= self.vy
        self.angle += randint(0 ,METEOR_FALLSPEED)

class MiniShip1_Red:
    def __init__(self, world, x, y):
        self.world = world 
        self.x = x
        self.y = y
        self.vx = 0
        self.vy = 0
        self.angle = -90
    
    def move(self):
        self.vx = MOVEMENT_SPEED
    
    def random_location(self):
        self.y = randint(0, self.world.height-1)
        self.x = -1000

    def animate(self, delta):
        self.move()
        if self.x >= self.world.width:
            self.random_location()
        self.x += self.vx

class MiniShip2_Blue:
    def __init__(self, world, x, y):
        self.world = world 
        self.x = x
        self.y = y
        self.vx = 0
        self.vy = 0
        self.angle = 90
    
    def move(self):
        self.vx = MOVEMENT_SPEED
    
    def random_location(self):
        self.y = randint(0, self.world.height-1)
        self.x = 1000

    def animate(self, delta):
        self.move()
        if self.x < 0:
            self.random_location()
        self.x -= self.vx


class World:
    NUM_METEOR = 7
    NUM_ENERGY = 4

    def __init__(self, width, height):
        self.width = width
        self.height = height
        self.player1_ship = Player1_Ship(self, 700, 100)
        self.player2_ship = Player2_Ship(self, 100,100)
        self.silver_energy = Silver_Energy(self, 0,self.height)
        self.enermy = Enermy(self, 400, self.height)
        self.miniShip1 = MiniShip1_Red(self,-1000,400)
        self.miniShip2 = MiniShip2_Blue(self,1000,400)
        self.P1_SCORE = 0
        self.P2_SCORE = 0 
        
        self.energy = []
        for i in range(World.NUM_ENERGY):
            energy = Energy(self, randint(0, self.width-1), self.height)
            self.energy.append(energy)

        self.meteor = [] 
        for k in range (World.NUM_METEOR):
            meteor = Meteor(self, randint(0, self.width-1), self.height)
            self.meteor.append(meteor)

    def animate(self, delta):
        self.player1_ship.animate(delta)
        self.player2_ship.animate(delta)
        self.enermy.animate(delta)
        self.silver_energy.animate(delta)
        self.miniShip1.animate(delta)
        self.miniShip2.animate(delta)


        if self.player1_ship.hit(self.miniShip1, 50):
            self.miniShip1.random_location()
            self.P1_SCORE += 15
        
        if self.player2_ship.hit(self.miniShip1, 50):
            self.miniShip1.random_location()
            self.P2_SCORE -= 5
        
        if self.player1_ship.hit(self.miniShip2, 50):
            self.miniShip2.random_location()
            self.P1_SCORE -= 5

        if self.player2_ship.hit(self.miniShip2, 50):
            self.miniShip2.random_location()
            self.P2_SCORE += 15
        
        if self.player1_ship.hit(self.silver_energy, 50):
            self.silver_energy.random_location()
            self.P1_SCORE += 3
        
        if self.player2_ship.hit(self.silver_energy, 50):
            self.silver_energy.random_location()
            self.P2_SCORE += 3

        
        if self.player1_ship.hit(self.enermy, 50):
            self.enermy.random_location()
            self.P1_SCORE -= 10
        
        if self.player2_ship.hit(self.enermy, 50):
            self.enermy.random_location()
            self.P2_SCORE -= 10
        
        for meteor in self.meteor:
            meteor.animate(delta)
            if self.player1_ship.hit(meteor, 50):
                meteor.random_location()
                self.P1_SCORE -= 2 
            if self.player2_ship.hit(meteor, 50):
                meteor.random_location()
                self.P2_SCORE -= 2

        for energy in self.energy:
            energy.animate(delta)
            if self.player1_ship.hit(energy, 50):
                energy.random_location()
                self.P1_SCORE += 1 
            if self.player2_ship.hit(energy, 50):
                energy.random_location()
                self.P2_SCORE += 1



    def on_key_press(self, key, key_modifiers):
        if key == arcade.key.UP:
            self.player1_ship.vy = MOVEMENT_SPEED
        elif key == arcade.key.DOWN:
            self.player1_ship.vy = -MOVEMENT_SPEED
        elif key == arcade.key.LEFT:
            self.player1_ship.vx = -MOVEMENT_SPEED
        elif key == arcade.key.RIGHT:
            self.player1_ship.vx = MOVEMENT_SPEED
        
        if key == arcade.key.W:
            self.player2_ship.vy = MOVEMENT_SPEED
        elif key == arcade.key.S:
            self.player2_ship.vy = -MOVEMENT_SPEED
        elif key == arcade.key.A:
            self.player2_ship.vx = -MOVEMENT_SPEED
        elif key == arcade.key.D:
            self.player2_ship.vx = MOVEMENT_SPEED

    def on_key_release(self, key, key_modifiers):
        if key == arcade.key.UP or key == arcade.key.DOWN:
            self.player1_ship.vy = 0
        elif key == arcade.key.LEFT or key == arcade.key.RIGHT:
            self.player1_ship.vx = 0

        if key == arcade.key.W or key == arcade.key.S:
            self.player2_ship.vy = 0
        elif key == arcade.key.A or key == arcade.key.D:
            self.player2_ship.vx = 0